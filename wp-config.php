<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'raideplo_inglass');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LTjz*P Hd_nOBksQJDdo)mWC:#wSFchmhH!KrQ |!9:I.DD3rqo:=ZrrIp{<!+@@');
define('SECURE_AUTH_KEY',  'F[g|!Ht!9[&l@_BsZ]ce<U3OO%=DVb}:u4tIH^[gxMaB)5;(xVW1sXQFcQyNYPyN');
define('LOGGED_IN_KEY',    '7{SoYspR,p}IM%XXbvm4fczD!.kWlNh<R6a4paO}%gZ`-fBktmw#Z0QLL}K4fG^0');
define('NONCE_KEY',        'MS1~9D2Vw 6En{d3qE!(KA,sqtY?6(ePmU Ch,QzX254{*PuNI!Z u{Y|tJAfwWh');
define('AUTH_SALT',        '9`pnw:Hd)[`ytiw^Uq#V&iDg.F;M77<pqKph0)fBI<164u_T9^T{fL-b6%!_gn=R');
define('SECURE_AUTH_SALT', ')gZ@0R=$y&Y+:r33LLOmbSYY,A1xQ0`>7zS#QZA2kJ(<e}FZaRII:Y6aq`8,xki2');
define('LOGGED_IN_SALT',   ' eS}w=HESv]1d8%J]lHRJ-{Sl}OwO4^v/GFAHy@,{m@_;|*csGC~GeZ*!^Rf|4*#');
define('NONCE_SALT',       'k{lp;7q(f%jq:5,^|@%YNHY!)UMgMsp.;V0B $b2 buqhT`T7,*12ajf?%:rIdFI');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

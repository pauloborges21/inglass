
<?php

    $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
    require_once( $parse_uri[0] . 'wp-load.php' );

    error_reporting(E_ERROR | E_PARSE);
    error_reporting(0);
    
    include 'con.php';

    $key = 'AIzaSyDHc5ncsAQ97iqMH2w_uceMt6YsgH4yhjQ';

    $query = "SELECT * FROM rai_lojas_inglass ORDER BY cidade";
    $result = mysqli_query($con, $query);
    $query2 = "SELECT * FROM rai_lojas_inglass";
    //echo $query2;
    $result2 = mysqli_query($con, $query2);

    //echo '<h3 style="font-size: 26px;line-height: 1.85;color: #572c5f;">Lojas mais próximas do cep: ' . utf8_encode($_POST['dist']) . '</h3>';
        
        while($distancia = mysqli_fetch_assoc($result2)){
        
        $latitude_destino = $distancia['latitude'];
        $longitude_destino = $distancia['longitude'];
        //echo $_POST['dist'];
        //echo 'lat dest: ' . $latitude_destino . '<br>';
        //echo 'long dest: ' . $longitude_destino . '<br>';
        $url_consulta = 'https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:' . $_POST['dist'] . '&sensor=false&key=' . $key;
        //echo $url_consulta;
        $dados_origem = json_decode(file_get_contents($url_consulta), true);
        //echo '<pre>';
        //var_dump($dados_origem);
        //echo '</pre>';
        $latidude_origem = $dados_origem['results'][0]['geometry']['location']['lat'];
        $longitude_origem = $dados_origem['results'][0]['geometry']['location']['lng'];
        //echo 'lat or: ' . $latidude_origem . '<br>';
        //echo 'long or: ' . $longitude_origem . '<br>';

        $info_dist = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . urlencode($latidude_origem) . ',' . urlencode($longitude_origem) . '&destinations=' . urlencode($latitude_destino) . ',' . urlencode($longitude_destino) . '&sensor=false&language=pt-BR';

        //echo $info_dist;

        $recupera_dist = json_decode(file_get_contents($info_dist), true);
        $dist_total = $recupera_dist['rows'][0]['elements'][0]['distance']['text'];
        $tempo = $recupera_dist['rows'][0]['elements'][0]['duration']['text'];
        $divide_dist = explode(' ', $dist_total);
        $pega_milha = $divide_dist[0];
        $pega_km = $pega_milha * 1.60934;
        $arredondado = round($pega_km);
        //echo '<br>distancia: ' . round($pega_km) . ' km';
        //echo '<br>cidade: ' . utf8_encode($distancia['cidade']);
        //echo '<hr>';
        
        
        $status_api = $recupera_dist['rows'][0]['elements'][0]['status'];


        $sql_insere = "UPDATE rai_lojas_inglass SET distancia = $arredondado, tempo = '$tempo' WHERE id = " . $distancia['id'];
        //echo $sql_insere;
        $sql_insere_exec = mysqli_query($con, $sql_insere);
        
        }
        
        
        
        if($sql_insere_exec){
          //$url_tema = 'http://localhost/git-caedu/wp-content/themes/caedu/';
          $sql_retorna = "SELECT * FROM rai_lojas_inglass ORDER BY LENGTH(distancia), distancia ASC";
          $sql_retorna_exec = mysqli_query($con, $sql_retorna);
        
        ?>

        <div class="row" id="todas-padrao">
            <div class="col small-12 large-12">
                <div class="col-inner">
                    <h3 style="font-size: 26px;line-height: 1.85;color: #572c5f;">Lojas mais próximas do cep: <?php echo $_POST['dist'] ?></h3>
                        <ul>

                        <?php while($distancia2 = mysqli_fetch_assoc($sql_retorna_exec)){ ?>

                            <li style="float:left; width:45%; list-style: none;">
                                <div style="float:left;margin-right:10px;height: 130px;">
                                    <img style="width:120px;" src="<?php echo $distancia2['url_imagem']; ?>">
                                </div>
                                <div>
                                    <p style="line-height:25px; padding-left:30px;">
                                        <span style="font-size:18px; color: #572c5f;"><?php echo $distancia2['nome_loja'] ?></span><br>
                                        <!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
                                        <a href="<?php //echo $distancia2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $distancia2['link_loja']; ?></a><br>-->
                                        <?php echo $distancia2['endereco']; ?>, <?php echo $distancia2['numero']; ?><br>
                                        <?php //echo $distancia2['bairro']; ?><!--<br>-->
                                        <?php echo $distancia2['cidade']; ?> - <?php echo $distancia2['estado']; ?><br>
                                        <strong><?php echo $distancia2['telefone1']; ?></strong>
                                    </p>
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                </div>
            </div>
        </div>

<?php } //if
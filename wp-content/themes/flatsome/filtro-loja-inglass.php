<?php
    
    $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
    require_once( $parse_uri[0] . 'wp-load.php' );

    include 'con.php';

    $query = "SELECT * FROM rai_lojas_inglass ORDER BY cidade";
    $result = mysqli_query($con, $query);
    $cidade_unica = mysqli_fetch_assoc($result);
    $query2 = "SELECT * FROM rai_lojas_inglass WHERE estado LIKE '%" . $_POST['search'] . "%' ORDER BY nome_loja ASC";
    //echo $query2;
    $result2 = mysqli_query($con, $query2);

    if($_POST['search'] == '' || $_POST['search'] == null){
    	echo '<script>location.reload();</script>';
    }
    ?>
    
    <?php
    if(mysqli_num_rows($result2) > 0 ){
      $url_tema = get_template_directory_uri();
    ?>

      <div class="row">
        <div class="col small-12 large-12">
            <div class="col-inner">
                <h3 style="font-size: 26px;line-height: 1.85;color: #572c5f;">Lojas encontradas</h3>
                    <ul>
                        <?php
                            while($output = mysqli_fetch_assoc($result2)){
                        ?>
                            <li style="float:left; width:45%; list-style: none;">
                                <div style="float:left;margin-right:10px;height: 130px;">
                                    <img style="width:120px;" src="<?php echo $output['url_imagem']; ?>">
                                </div>
                                <div>
                                    <p style="line-height:25px; padding-left:30px;">
                                        <span style="font-size:18px; color: #572c5f;"><?php echo $output['nome_loja'] ?></span><br>
                                        <!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
                                        <a href="<?php //echo $output['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $output['link_loja']; ?></a><br>-->
                                        <?php echo $output['endereco']; ?>, <?php echo $output['numero']; ?><br>
                                        <?php //echo $output['bairro']; ?><!--<br>-->
                                        <?php echo $output['cidade']; ?> - <?php echo $output['estado']; ?><br>
                                        <strong><?php echo $output['telefone1']; ?></strong>
                                    </p>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
            </div>
        </div>
    </div>
    <?php
	
    }else{
		echo 'nada encontrado';
	}
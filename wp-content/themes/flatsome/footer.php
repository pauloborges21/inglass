<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>
	<form method="post" action="#">
		<input type="email" name="email" />
		<input type="submit" name="enviar" value="cadastrar">
	</form>
</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<script src="<?php echo get_template_directory_uri() ?>/assets/js/cidades-estados-1.2-utf8.js"></script>

<script language="JavaScript" type="text/javascript" charset="utf-8">
  new dgCidadesEstados({
    cidade: document.getElementById('cidade1'),
    estado: document.getElementById('estado1')
  })
</script>

<?php wp_footer(); ?>

</body>
</html>

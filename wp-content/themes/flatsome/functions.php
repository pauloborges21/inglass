<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */

function facebook_params(){
    global $wpdb;
    $termo = '';
    
	if(isset($_GET['utm_term'])){
		$data = date('YmdHis');
		$termo = $_GET['utm_term'];
		$nome_cookie = 'inglass';
		
		if(!isset($_COOKIE[$nome_cookie])){
			ob_start();
			$valor_cookie = 'cookie_rai_inglass_' . $data;
			$expira = time()+3600;
			setcookie($nome_cookie, $valor_cookie, $expira, '/');
			ob_end_flush();
			$wpdb->insert(
			"rai_params",
			array("cookie" => "$valor_cookie",
			  "utm_term" => "$termo",
			  "email" => ""
			));

		}
	}else{ // cookie esta setado
		$nome_cookie = 'inglass';
		$email = $_POST['email'];
		//echo '<script>alert("Cookie setado nome: '.$_COOKIE[$nome_cookie].'");</script>';
		//echo '<script>alert("'.site_url().'");</script>';
		if($email != '' && $email != null){
		$wpdb->update('rai_params', array(
		        'email' => $email
		    ), array(
		        'cookie' => $_COOKIE[$nome_cookie]
		    )
		);
		}
	}
}

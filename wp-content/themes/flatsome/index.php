<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>

<div id="content" class="blog-wrapper blog-archive page-wrapper">
		<?php get_template_part( 'template-parts/posts/layout', get_theme_mod('blog_layout','right-sidebar') ); ?>
</div><!-- .page-wrapper .blog-wrapper -->

<?php echo wp_link_pages();?>
<?php echo wp_pagenavi();?>

<?php get_footer(); ?>
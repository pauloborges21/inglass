<?php get_header(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&key=AIzaSyA21LM1AfaEtjRiDHRtdXBRajuy_R7UX2E"></script>

<?php echo do_shortcode( '[block id="header-onde-comprar"]' ); ?>

<div class="section-content relative">

	<div class="row">
		<div class="col small-12 large-12">
			<div class="col-inner">
				<h1 style="font-size: 250%;line-height: 0.9;color: #572c5f;">Onde encontrar</h1>
				<p>Use o nosso localizador para encontrar a loja mais próxima de você.
</p>
				<hr style="width: 60px;height: 4px;background-color: #a73a64; opacity:1;" />
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col small-4 large-4">
			<div class="col-inner">
				<h2 class="h2small">Selecione o Estado</h2>
				<select name="" style="height:50px; margin-bottom:-1em;" onchange="filterMarkers(this.value);" id="select-cidade">
					<option value="">Estado</option>
					<?php
						include 'con.php';
		                $query_cidade = "SELECT DISTINCT estado FROM rai_lojas_inglass ORDER BY estado ASC";
		                $result_cidade = mysqli_query($con, $query_cidade);
		                while($place = @mysqli_fetch_assoc($result_cidade)){
		            ?>

		            <option value="<?php echo $place['estado']; ?>"><?php echo $place['estado']; ?></option>

		            <?php
		                }
		            ?>

				</select>
				<span id="esconde-cep"><a id="mostrar-cep" style="cursor: pointer;"> <br>ou clique aqui para buscar pelo CEP</a></span>
			</div>
		</div>

		<!--<div class="col small-4 large-4" id="esconde-cep">
			<div class="col-inner">
				<a id="mostrar-cep" style="cursor: pointer;">Buscar pelo CEP?</a>
			</div>
		</div>-->

		<script>
			$('#esconde-cep').click(function(){
				$('#esconde-cep').hide();
				$('#area-cep').show();
				$('#btn-cep').show();
			});
		</script>


			<div class="col small-4 large-4" id="area-cep" style="display:none;">
				<div class="col-inner">
					<p style="font-size:18px; color: #572c5f;margin-bottom: 5px;">Buscar pelo CEP <a id="volta-padrao" style="cursor: pointer;">[esconder]</a></p>
					<input type="text" value="" style="height:50px;" id="cep" />
				</div>
			</div>

			<script>
				$('#volta-padrao').click(function(){
					$('#esconde-cep').show();
					$('#area-cep').hide();
					$('#btn-cep').hide();
				});
			</script>

			<div class="col small-4 large-4" id="btn-cep" style="display:none;">
				<div class="col-inner">
					<button id="envia-cep" style="width: 170px;height: 50px;border: solid 2px #c0396d; margin-top:35px; font-size:12px;">Pesquisar&nbsp;&nbsp;<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linha-btn-pesquisar.jpg"></button>
				</div>
			</div>

	</div>

	<div class="row">
		<div class="col small-12 large-12">
			<div class="col-inner">
				<div id="map-canvas" style="width: 100%; height: 650px;"></div>
					<script>
						var gmarkers1 = [];
						var markers1 = [];
						var infowindow = new google.maps.InfoWindow({
						    content: ''
						});

						<?php

							//include 'con.php';
							$query = "SELECT * FROM rai_lojas_inglass";
							$result = mysqli_query($con, $query);
							$lugar = '';

						      while($row = @mysqli_fetch_assoc($result)){
						        //$row_js = implode(",", $row);
						        $lugar .= '["' . $row['id'] . '",
						        			"' . $row['nome_loja'] . '",
						        			"' . $row['endereco'] . '",
						        			"' . $row['numero'] . '",
						        			"' . $row['complemento'] . '",
						        			"' . $row['bairro'] . '",
						        			"' . $row['cidade'] . '",
						        			"' . $row['cidade_flat'] . '",
						        			"' . $row['estado'] . '",
						        			"' . $row['cep'] . '",
						        			"' . $row['pais'] . '",
						        			"' . $row['telefone1'] . '",
						        			"' . $row['telefone2'] . '",
						        			"' . $row['telefone3'] . '",
						        			"' . $row['celular'] . '",
						        			"' . $row['horario_lojas'] . '",
						        			"' . $row['email'] . '",
						        			"' . $row['link_loja'] . '",
						        			' . $row['latitude'] . ',
						        			' . $row['longitude'] . ',
						        			"' . $row['url_imagem'] . '"],';
						      }
						      echo 'markers1 = [' . $lugar . ']';

						    ?>

						    function initialize() {
							    var center = new google.maps.LatLng(-15.647825,-47.808008);
							    var mapOptions = {
							        zoom: 4,
							        center: center,
							        mapTypeId: google.maps.MapTypeId.roadmap
							    };

							    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
							    for (i = 0; i < markers1.length; i++) {
							        addMarker(markers1[i]);
							    }
							}

						    /**
							 * Function to add marker to map
							 */

							function addMarker(marker) {
							    var category = marker[8];
							    var title = marker[1];
							    var pos = new google.maps.LatLng(marker[18], marker[19]);
							    var content = marker[1] + '<br>' + marker[2] + ', ' + marker[3] + '<br>' + marker[6] + '/' + marker[8] + ' - CEP: ' + marker[9] + '<br>Telefone: ' + marker[11];


								var image = {
							      url: "http://www.myiconfinder.com/uploads/iconsets/256-256-a5485b563efc4511e0cd8bd04ad0fe9e.png", // url
							      scaledSize: new google.maps.Size(60, 60) // scaled size
							      //origin: new google.maps.Point(0,0), // origin
							      //anchor: new google.maps.Point(0, 0) // anchor
							  };

							    marker1 = new google.maps.Marker({
							        title: title,
							        position: pos,
							        category: category,
							        map: map,
							        icon: image
							    });

							    gmarkers1.push(marker1);

							    // Marker click listener
							    google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
							        return function () {
							            console.log('Gmarker 1 gets pushed');
							            infowindow.setContent(content);
							            infowindow.open(map, marker1);
							            map.panTo(this.getPosition());
							            map.setZoom(15);
							        }
							    })(marker1,content));
							}


							/**
							 * Function to filter markers by category
							 */

							filterMarkers = function (category) {
							    for (i = 0; i < markers1.length; i++) {
							        marker = gmarkers1[i];
							        // If is same category or category not picked
							        if (marker.category == category || category.length === 0) {
							            marker.setVisible(true);
							            map.setZoom(7);
										map.panTo(marker.position);
							        }
							        // Categories don't match
							        else {
							            marker.setVisible(false);
							        }
							    }
							}

							// Init map
							initialize();

						</script>
			</div>
		</div>
	</div>

	<div class="row" id="todas-padrao">
		<div class="col small-12 large-12">
			<div class="col-inner">
				<h2 class="h2small">Vidraçarias Parceiras</h2><br>
					<h3>Distrito Federal</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'DF' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col small-12 large-12">
	            <div class="col-inner">

	                <h3>Mato Grosso do Sul</h3><hr>
	                <ul>
	                    <?php
	                    $query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'MS'";
	                    $result2 = mysqli_query($con, $query2);
	                    //$conta_cidade = mysqli_num_rows($result2);
	                    //echo 'conta cidade: ' . $conta_cidade;
	                    while($row2 = @mysqli_fetch_assoc($result2)){
	                        ?>
	                        <li style="float:left; width:45%; list-style: none;">
	                            <div style="float:left;margin-right:10px;height: 160px;">
	                                <img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
	                            </div>
	                            <div>
	                                <p style="line-height:25px; padding-left:30px;">
	                                    <span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
	                                    <!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
											<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
	                                    <?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
	                                    <?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
	                                    <?php //echo $row2['bairro']; ?><!--<br>-->
	                                    <?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>
	                                    <strong>
	                                        <?php
	                                        echo $row2['telefone1'];
	                                        echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
	                                        ?></strong><br>
	                                    <?php echo $row2['email']; ?>
	                                </p>
	                            </div>
	                        </li>
	                    <?php } ?>
	                </ul>
	            </div>
	        </div>

			<div class="col small-12 large-12">
				<div class="col-inner">

					<h3>Minas Gerais</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'MG' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col small-12 large-12">
				<div class="col-inner">

					<h3>Paraná</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'PR' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col small-12 large-12">
				<div class="col-inner">

					<h3>Rio de Janeiro</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'RJ' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col small-12 large-12">
				<div class="col-inner">

					<h3>Rio Grande do Sul</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'RS' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col small-12 large-12">
				<div class="col-inner">

					<h3>São Paulo</h3><hr>
					<ul>
						<?php
						$query2 = "SELECT * FROM rai_lojas_inglass WHERE estado = 'SP' ORDER BY nome_loja ASC";
						$result2 = mysqli_query($con, $query2);
						//$conta_cidade = mysqli_num_rows($result2);
						//echo 'conta cidade: ' . $conta_cidade;
						while($row2 = @mysqli_fetch_assoc($result2)){
						?>
							<li style="float:left; width:45%; list-style: none;">
								<div style="float:left;margin-right:10px;height: 160px;">
									<img style="width:120px;" src="<?php echo $row2['url_imagem']; ?>">
								</div>
								<div>
									<p style="line-height:25px; padding-left:30px;">
										<span style="font-size:18px; color: #572c5f;"><?php echo $row2['nome_loja'] ?></span><br>
										<!--<img src="<?php //echo get_template_directory_uri(); ?>/assets/img/link-icon.png">&nbsp;&nbsp;
										<a href="<?php //echo $row2['link_loja']; ?>" target="_blank" style="color: #7abde9;"><?php //echo $row2['link_loja']; ?></a><br>-->
										<?php echo $row2['endereco']; ?>, <?php echo $row2['numero']; ?>
										<?php echo $row2['complemento'] == '' ? '' :  ' - ' . $row2['complemento']; ?><br>
										<?php //echo $row2['bairro']; ?><!--<br>-->
										<?php echo $row2['cidade']; ?> - <?php echo $row2['estado']; ?><br>										
										<strong>
											<?php 
												echo $row2['telefone1']; 
												echo $row2['telefone2'] == '' ? '' :  ' / ' . $row2['telefone2'];
											?></strong><br>
										<?php echo $row2['email']; ?> 
									</p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
	</div>


<!-- I: Chamada Ajax pra mostrar só as lojas da cidade selecionada -->
<div id="show"></div>
<script>
$("#select-cidade").change(function(){

    var txt = $(this).val();
    //var hora = $('#mes').value;

    $('#show').html('');
    $.ajax({
        url: "<?php bloginfo('template_directory'); ?>/filtro-loja-inglass.php",
        method: "post",
        data:{search:txt},
        dataType: "text",
        success:function(data){
            $('#show').html(data);
        }
    });

    $("#todas-padrao").hide();

});
</script>
<!-- F: Chamada Ajax pra mostrar só as lojas da cidade selecionada -->

<!-- I: Chamada Ajax pra mostrar a loja mais proxima de acordo com o CEP enviado -->
<div id="por-distancia" style="margin:0 auto;"></div>
<script>
$("#envia-cep").click(function(){
    $('#por-distancia').show();
    $('#por-distancia').html('<div class="section-content relative" style="text-align:center;"><div class="row" style="text-align:center;"><div class="col small-12 large-12" style="text-align:center;"><div class="col-inner" style="text-align:center;"><img src="<?php bloginfo('template_directory'); ?>/assets/img/loading-inglass.gif" style="text-align:center;"> Aguarde enquanto procuramos a loja mais próxima do CEP informado...</div></div></div></div>');
    $("#todas-padrao").hide();
    $('#show').hide();

    var txt = $('#cep').val();
    var geocoder = new google.maps.Geocoder();


    $.ajax({
        url: "<?php bloginfo('template_directory'); ?>/filtro-cep-inglass.php",
        method: "post",
        data:{dist:txt},
        dataType: "text",
        success:function(data){

            $('#por-distancia').html(data);
            $("#todas-padrao").hide();
            $('#show').hide();
        }
    });
});
</script>
<!-- F: Chamada Ajax pra mostrar a loja mais proxima de acordo com o CEP enviado -->

</div>

<?php get_footer(); ?>

<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

get_header();

?>

<?php echo do_shortcode('[block id="banner-single-post-page"]'); ?>
<div style="background-color:#f7f7f7;" id="content" class="blog-wrapper blog-single page-wrapper">
	<?php get_template_part( 'template-parts/posts/layout', get_theme_mod('blog_post_layout','right-sidebar') ); ?>
</div><!-- #content .page-wrapper -->

<?php wp_link_pages();?>

<?php get_footer();?>

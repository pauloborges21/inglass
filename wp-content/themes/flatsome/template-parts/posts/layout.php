<?php
	do_action('flatsome_before_blog');
?>

<?php if(!is_single() && flatsome_option('blog_featured') == 'top'){ get_template_part('template-parts/posts/featured-posts'); } ?>
<div class="row align-center">
	<div class="large-12 col divpost">
<?php echo do_shortcode('[block id="breadcrumbs-and-social-icons"]'); ?>

	<?php if(!is_single() && flatsome_option('blog_featured') == 'content'){ get_template_part('template-parts/posts/featured-posts'); } ?>


	<?php
		if(is_single()){
			get_template_part( 'template-parts/posts/single');
				echo do_shortcode('[block id="related-posts"]');
			comments_template();
		} else{
			get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style') );
		}
	?>

</div> <!-- .large-12 -->

</div><!-- .row -->

<?php do_action('flatsome_after_blog');
